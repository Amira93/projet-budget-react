# Application de gestion de budget

- Projet: ‘’Gestion de budget’’
- Date :  09 juillet 2021
- Réalisateur :  Amira Habi

## Description:
Ce projet entre dans le cadre de la formation ‘Développeur Web’ de l’école Simplon.co Lyon. Le concept de cette formation est de suivre des cours théoriques et des ateliers pratiques puis de les valider par des projets personnels. 
Ce projet consiste a réaliser une application de gestion de budget avec Node.JS et React. Tout d'abord, j'ai commencé a créer un dépot git pour mon back et mon front.

## Technologies utilisées:

* Node JS
* Javascript
* Express
* Super Test
* React
* MySQL

## Fonctionnalités:

* Consulter les opérations
* Ajouter une opération
* Supprimer une opération
* Rechercher une opération par mois
* Voir le total

## Diagramme de classe
![wireframe budget](public/MCD_Budget.png)


## les lien de gitlab:
- Lien Front-end: https://gitlab.com/Amira93/projet-budget-react
- Lien Back-end: https://gitlab.com/Amira93/projet-budget
## le lien de heruko:
https://projet-budget-api.herokuapp.com/
## Le lien de l'application complete (front-end a deploié sur netfly):
https://60eda5168f632e00075cb84f--laughing-kalam-9cc74c.netlify.app/



