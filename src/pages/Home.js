import axios from "axios";
import { useEffect, useState } from "react"
import { Budget } from "../compenent/Budget";
import { FormBudget } from "../compenent/FomBudget";
import { SearchBar } from "../compenent/SearchBar";

export function Home(){

  const [budget, setBudget]= useState([]);

  async function fetchBudget(){
    const response = await axios.get('https://projet-budget-api.herokuapp.com/api/budget');
    setBudget(response.data);
  }

  async function deleteBudget(id){
    await axios.delete('https://projet-budget-api.herokuapp.com/api/budget/' +id)
    setBudget(budget.filter(item => item.id !==id))
  }

  async function addBudget(budgets){
      const response = await axios.post('https://projet-budget-api.herokuapp.com/api/budget/', budgets);
      setBudget([
          ...budget,
          response.data
      ])
  }
 
    const handleSearch= async (search) =>{ 
    const response = await axios.get('https://projet-budget-api.herokuapp.com/api/budget?search=' +search);
    setBudget(response.data);
  }

  useEffect(() => {
    fetchBudget();
}, []);

function prixTotal() {
  let total = 0;
  for (const bud of budget) {

    total += Number(bud.montant)

  }
  return total

}

    return (
      <div>
         <header className= "header">
                <h1> Welcome to my budget list </h1>
                <SearchBar onSearch={handleSearch} />
        </header>
         <section className= "row">
          <table className ="table table-primary table-striped" id= "table-style">
            <thead>
                <tr >
                  <th scope="col">#</th>
                  <th scoope="col">Titre</th> 
                  <th scoope="col">Categorie</th> 
                  <th scoope="col">Montant</th> 
                  <th scoope="col">Date</th> 
                 </tr>
              </thead>

            <tbody>
          {budget.map(item=> 
            <Budget key= {item.id}
            budget={item}
            onDelete={deleteBudget}/>
          )}
          <tr className="table-danger">
            <th className="table-danger">Total: </th>
            <th className="table-danger"></th>
            <th className="table-danger"></th>
            <th className="table-danger">{prixTotal()}€</th>
            <th className="table-danger"></th>
          </tr>
           </tbody>
        </table>
       </section>
       <FormBudget onFormSubmit={addBudget}/>
      </div>
  )

  }