
export function Budget({budget, onDelete})
{
    return (
            
                <tr>
                  <th >{budget.id}</th>
                  <td>{budget.titre}</td>
                  <td>{budget.categorie}</td>
                  <td className= {budget.montant < 0 ? 'negative' :'positive'}>{budget.montant}€</td>
                  <td> { new Intl.DateTimeFormat('SDATEw').format(new Date(budget.date))}<button id = "delete" className="btn btn-primary" onClick={() => onDelete(budget.id)}>X</button></td>
                 
                </tr>
                 
               
    )
}
