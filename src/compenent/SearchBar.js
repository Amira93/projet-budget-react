
export function SearchBar({ onSearch }) {

    const handleChange = (event) => {
        onSearch(event.target.value);
    }
    return (
        <div className="input-group">
            <div className="form-outline">
                <input type="search" id="form" className="form-control" placeholder="search" onChange={handleChange}/>
            </div>
            <button type="button" className="btn btn-primary" id = "search">
            <i className="bi bi-search"></i>
            </button>
        </div>
    )
}

