import { useState } from "react"


const initialState = {
    titre: '',
    categorie: '',
    montant: '',
    date: ''
}

export function FormBudget({onFormSubmit, budget = initialState}){
    const [form, setForm] = useState(budget);

    function handleChange(event){
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }
    function handleSubmit(event){
        event.preventDefault();
        onFormSubmit(form);
    }
    return (
        <form className="col-4 " onSubmit={handleSubmit}>
            <label>Titre :</label>
            <input className="form-control"  type="text" name="titre" onChange={handleChange} value={form.titre}/>
            <label>Catgorie :</label>
            <input className="form-control" required type="text" name="categorie" onChange={handleChange} value={form.catgorie}/>
            <label>Montant :</label>
            <input className="form-control" required type="number" name="montant" onChange={handleChange} value={form.montant}/>
            <label>Date :</label>
            <input className="form-control" required type="date" name="date" onChange={handleChange} value={form.date}/>
            <button className="btn btn-primary">Submit</button>
        </form>
    )
}